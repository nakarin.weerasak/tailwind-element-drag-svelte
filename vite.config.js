import { defineConfig } from 'vite'
import svelte from '@sveltejs/vite-plugin-svelte'
import svgLoader from 'vite-svg-loader'
import path from 'path'

// https://vitejs.dev/config/
export default defineConfig({
    base: "./",
    build: {
        outDir: './build'
    },
    resolve: {
        alias: {
            'src': path.resolve(__dirname, './src/')
        }
    },
    plugins: [svelte(), svgLoader()]
})