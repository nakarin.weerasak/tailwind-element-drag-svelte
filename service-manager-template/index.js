const express = require('express')
const cors = require('cors');
const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth2').Strategy;
const app = express()


const googleAuthKey = {
    "client_id": "1049656733-i9vpaj4k9vv8p059asd85p47snl0rucs.apps.googleusercontent.com",
    "project_id": "grapesjs-svelte-drag",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_secret": "1pqgqmupAoB3csPC0Y83DuO-"
}



const { templateList } = require('./template-dt/template')

passport.use(new GoogleStrategy({
    clientID: googleAuthKey['client_id'],
    clientSecret: googleAuthKey['client_secret'],
    callbackURL: "http://localhost:3000/auth/google/callback",
    passReqToCallback: true
},
    function (request, accessToken, refreshToken, profile, cb) {
        return cb(null, profile);
    }
));

app.use(express.json())
app.use(express.urlencoded())
app.use(cors())

passport.serializeUser(function (user, done) {
    done(null, user.id);
});


app.use('/', passport.authenticate('google', {
    scope:
        ['email', 'profile']
}
))


app.use('/builder', express.static('../build'));

app.get('/auth/google/callback',
    passport.authenticate('google', { failureRedirect: '/' }),
    function (req, res) {
        // Successful authentication, redirect home.
        res.redirect('/builder');
    });
app.get('/service/store', function (req, res) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', 'true')
    res.send({ "gjs-components": [{ "tagName": "link", "void": true, "attributes": { "href": "https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css", "rel": "stylesheet" } }], "gjs-html": `<section class="text-gray-600 body-font"><div class="container px-5 py-24 mx-auto flex items-center md:flex-row flex-col"><div class="flex flex-col md:pr-10 md:mb-0 mb-6 pr-0 w-full md:w-auto md:text-left text-center"><h2 class="text-xs text-indigo-500 tracking-widest font-medium title-font mb-1">ROOF PARTY POLAROID</h2><h1 class="md:text-3xl text-2xl font-medium title-font text-gray-900">Master Cleanse Reliac Heirloom</h1></div><div class="flex md:ml-auto md:mr-0 mx-auto items-center flex-shrink-0 space-x-4"><button type="button" class="bg-gray-100 inline-flex py-3 px-5 rounded-lg items-center hover:bg-gray-200 focus:outline-none">Send</button><button type="button" class="bg-gray-100 inline-flex py-3 px-5 rounded-lg items-center hover:bg-gray-200 focus:outline-none">Send</button></div></div></section>`, "gjs-css": `* { box-sizing: border-box; } body {margin: 0;}`, "gjs-assets": [], "gjs-styles": [] })
})

app.post('/service/store', function (req, res) {
    res.send('Hello World')
})

const PORT = process.env.PORT || 3000
app.listen(PORT, '0.0.0.0', () => {
    console.log(`app listen on http://localhost:${PORT}`);
})