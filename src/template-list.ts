import type { TemplateModel } from "./models/template.model";

export const footerListTemplate: Array<TemplateModel> = [
    // {
    //     id: Date.now().toString(),
    //     class: "flex justify-center text-red-900",
    //     label: `<img class="" src="https://grapesjs.com/docs/logo.png"/>`,
    //     content: `<div
    //     class="py-16 bg-gray-900 leading-6 text-black border-r-0 border-l-0 border-t border-b border-gray-800 border-solid box-border"
    //     style="border-width: 0px;"
    //   >
    //     <div
    //       class="px-4 mx-auto w-full text-black lg:max-w-screen-lg xl:max-w-screen-xl xl:px-12 sm:max-w-screen-sm md:max-w-screen-md"
    //     >
    //       <div class="lg:flex">
    //         <div class="flex mb-16 w-full lg:mb-0 lg:w-1/2">
    //           <div class="px-6 w-full lg:w-1/2">
    //             <ul class="p-0 m-0" style="list-style: none;">
    //               <li
    //                 id="f1"
    //                 class="text-left"
    //                 style="list-style: outside none none;"
    //               >
    //                 <a
    //                   class="text-sm leading-none text-gray-100 cursor-pointer md:text-base md:leading-6 hover:text-red-600"
    //                   href="/components"
    //                   style="list-style: outside none none;"
    //                   >Components</a
    //                 >
    //               </li>
    //               <li
    //                 id="f2"
    //                 class="mt-6 text-left"
    //                 style="list-style: outside none none;"
    //               >
    //                 <a
    //                   class="text-sm leading-none text-gray-100 cursor-pointer md:text-base md:leading-6 hover:text-red-600"
    //                   href="/templates"
    //                   style="list-style: outside none none;"
    //                   >Templates</a
    //                 >
    //               </li>
    //               <li
    //                 id="f3"
    //                 class="mt-6 text-left"
    //                 style="list-style: outside none none;"
    //               >
    //                 <a
    //                   class="text-sm leading-none text-gray-100 cursor-pointer md:text-base md:leading-6 hover:text-red-600"
    //                   href="/pricing"
    //                   style="list-style: outside none none;"
    //                   >Pricing</a
    //                 >
    //               </li>
    //               <li
    //                 id="f4"
    //                 class="mt-6 text-left"
    //                 style="list-style: outside none none;"
    //               >
    //                 <a
    //                   class="text-sm leading-none text-gray-100 cursor-pointer md:text-base md:leading-6 hover:text-red-600"
    //                   href="/pricing#faq"
    //                   style="list-style: outside none none;"
    //                   >FAQ</a
    //                 >
    //               </li>
    //               <li
    //                 id="f5"
    //                 class="mt-6 text-left"
    //                 style="list-style: outside none none;"
    //               >
    //                 <a
    //                   class="text-sm leading-none text-gray-100 cursor-pointer md:text-base md:leading-6 hover:text-red-600"
    //                   href="/docs"
    //                   style="list-style: outside none none;"
    //                   >Documentation</a
    //                 >
    //               </li>
    //             </ul>
    //           </div>
    //           <div class="px-6 w-full lg:w-1/2">
    //             <ul class="p-0 m-0" style="list-style: none;">
    //               <li
    //                 id="f6"
    //                 class="text-left"
    //                 style="list-style: outside none none;"
    //               >
    //                 <a
    //                   class="text-sm leading-none text-gray-100 cursor-pointer md:text-base md:leading-6 hover:text-red-600"
    //                   href="/components"
    //                   style="list-style: outside none none;"
    //                   >Free components</a
    //                 >
    //               </li>
    //               <li
    //                 id="f7"
    //                 class="mt-6 text-left"
    //                 style="list-style: outside none none;"
    //               >
    //                 <a
    //                   class="text-sm leading-none text-gray-100 cursor-pointer md:text-base md:leading-6 hover:text-red-600"
    //                   href="/blog"
    //                   style="list-style: outside none none;"
    //                   >Blog</a
    //                 >
    //               </li>
    //               <li
    //                 id="f8"
    //                 class="mt-6 text-left"
    //                 style="list-style: outside none none;"
    //               >
    //                 <a
    //                   class="text-sm leading-none text-gray-100 cursor-pointer md:text-base md:leading-6 hover:text-red-600"
    //                   href="/changelog"
    //                   style="list-style: outside none none;"
    //                   >Changelog</a
    //                 >
    //               </li>
    //             </ul>
    //           </div>
    //         </div>
    //         <div class="flex w-full lg:w-1/2">
    //           <div class="px-6 w-full lg:w-1/2">
    //             <ul class="p-0 m-0" style="list-style: none;">
    //               <li
    //                 id="f9"
    //                 class="text-left"
    //                 style="list-style: outside none none;"
    //               >
    //                 <a
    //                   href="/terms-and-conditions#privacyPolicy"
    //                   class="text-sm leading-none text-gray-100 cursor-pointer md:text-base md:leading-6 hover:text-red-600"
    //                   style="list-style: outside none none;"
    //                   >Privacy policy</a
    //                 >
    //               </li>
    //               <li
    //                 id="f10"
    //                 class="mt-6 text-left"
    //                 style="list-style: outside none none;"
    //               >
    //                 <a
    //                   class="text-sm leading-none text-gray-100 cursor-pointer md:text-base md:leading-6 hover:text-red-600"
    //                   href="/terms-and-conditions"
    //                   style="list-style: outside none none;"
    //                   >Terms of service</a
    //                 >
    //               </li>
    //             </ul>
    //           </div>
    //           <div class="flex flex-col justify-between px-6 w-full lg:w-1/2">
    //             <div class="flex items-center mb-6">
    //               <a
    //                 id="f11"
    //                 href="https://github.com/AlphaSquadTech"
    //                 aria-label="github link"
    //                 class="cursor-pointer"
    //                 ><div class="text-gray-100 hover:text-red-600">
    //                   <svg
    //                     class="block align-middle"
    //                     xmlns="http://www.w3.org/2000/svg"
    //                     width="24"
    //                     height="24"
    //                     viewBox="0 0 24 24"
    //                     fill="none"
    //                     stroke="currentColor"
    //                     stroke-width="1.5"
    //                     stroke-linecap="round"
    //                     stroke-linejoin="round"
    //                   >
    //                     <path
    //                       d="M9 19c-5 1.5-5-2.5-7-3m14 6v-3.87a3.37 3.37 0 0 0-.94-2.61c3.14-.35 6.44-1.54 6.44-7A5.44 5.44 0 0 0 20 4.77 5.07 5.07 0 0 0 19.91 1S18.73.65 16 2.48a13.38 13.38 0 0 0-7 0C6.27.65 5.09 1 5.09 1A5.07 5.07 0 0 0 5 4.77a5.44 5.44 0 0 0-1.5 3.78c0 5.42 3.3 6.61 6.44 7A3.37 3.37 0 0 0 9 18.13V22"
    //                       class=""
    //                     ></path>
    //                   </svg></div></a
    //               ><a
    //                 id="f12"
    //                 href="https://twitter.com/tailwinduikit"
    //                 aria-label="Twitter link"
    //                 class="cursor-pointer"
    //                 ><div class="pl-4">
    //                   <svg
    //                     class="block text-gray-100 align-middle hover:text-red-600"
    //                     xmlns="http://www.w3.org/2000/svg"
    //                     width="24"
    //                     height="24"
    //                     viewBox="0 0 24 24"
    //                     fill="none"
    //                     stroke="currentColor"
    //                     stroke-width="1.5"
    //                     stroke-linecap="round"
    //                     stroke-linejoin="round"
    //                   >
    //                     <path
    //                       d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z"
    //                       class=""
    //                     ></path>
    //                   </svg></div
    //               ></a>
    //             </div>
    //             <div class="relative w-32">
    //               <svg
    //                 xmlns="http://www.w3.org/2000/svg"
    //                 class="block absolute inset-0 my-auto mr-auto ml-3 text-gray-100 align-middle"
    //                 width="20"
    //                 height="20"
    //                 viewBox="0 0 24 24"
    //                 stroke-width="1.5"
    //                 stroke="currentColor"
    //                 fill="none"
    //                 stroke-linecap="round"
    //                 stroke-linejoin="round"
    //               >
    //                 <path stroke="none" d="M0 0h24v24H0z" fill="none" class=""></path>
    //                 <path
    //                   d="M12 3c.132 0 .263 0 .393 0a7.5 7.5 0 0 0 7.92 12.446a9 9 0 1 1 -8.313 -12.454z"
    //                   class=""
    //                 ></path></svg
    //               ><svg
    //                 xmlns="http://www.w3.org/2000/svg"
    //                 class="block absolute inset-0 my-auto mr-3 ml-auto text-gray-100 align-middle pointer-events-none"
    //                 width="16"
    //                 height="16"
    //                 viewBox="0 0 24 24"
    //                 stroke-width="1.5"
    //                 stroke="currentColor"
    //                 fill="none"
    //                 stroke-linecap="round"
    //                 stroke-linejoin="round"
    //               >
    //                 <path stroke="none" d="M0 0h24v24H0z" fill="none" class=""></path>
    //                 <polyline points="6 9 12 15 18 9" class=""></polyline></svg
    //               ><select
    //                 aria-label="Select Mode"
    //                 class="flex items-center py-2 pr-0 pl-10 m-0 w-full h-12 text-sm leading-5 text-gray-100 normal-case whitespace-pre bg-gray-900 rounded border border-gray-100 border-solid appearance-none cursor-default box-border"
    //               >
    //                 <option value="auto" class="whitespace-no-wrap">Auto</option>
    //                 <option value="light" class="whitespace-no-wrap">Light</option>
    //                 <option value="dark" class="whitespace-no-wrap">Dark</option>
    //               </select>
    //             </div>
    //           </div>
    //         </div>
    //       </div>
    //     </div>
    //   </div>
    //   `,
    // }
    , {
        id: Date.now().toString(),
        class: "flex justify-center text-red-900",
        label: `<img class="" src="https://grapesjs.com/docs/logo.png"/>`,
        content: {
            type: "comp-with-js",
            script: `
            function dropdownHandler(element) {
                let single = element.getElementsByTagName("ul")[0];
                single.classList.toggle("hidden");
            }
            function MenuHandler(el, val) {
                let MainList = el.parentElement.parentElement.getElementsByTagName("ul")[0];
                let closeIcon = el.parentElement.parentElement.getElementsByClassName("close-m-menu")[0];
                let showIcon = el.parentElement.parentElement.getElementsByClassName("show-m-menu")[0];
                if (val) {
                    MainList.classList.remove("hidden");
                    el.classList.add("hidden");
                    closeIcon.classList.remove("hidden");
                } else {
                    showIcon.classList.remove("hidden");
                    MainList.classList.add("hidden");
                    el.classList.add("hidden");
                }
            }
            // ------------------------------------------------------
            let sideBar = document.getElementById("mobile-nav");
            let menu = document.getElementById("menu");
            let cross = document.getElementById("cross");
            const sidebarHandler = (check) => {
                if (check) {
                    sideBar.style.transform = "translateX(0px)";
                    menu.classList.add("hidden");
                    cross.classList.remove("hidden");
                } else {
                    sideBar.style.transform = "translateX(-100%)";
                    menu.classList.remove("hidden");
                    cross.classList.add("hidden");
                }
            };
            let list = document.getElementById("list");
            let chevrondown = document.getElementById("chevrondown");
            let chevronup = document.getElementById("chevronup");
            const listHandler = (check) => {
                if (check) {
                    list.classList.remove("hidden");
                    chevrondown.classList.remove("hidden");
                    chevronup.classList.add("hidden");
                } else {
                    list.classList.add("hidden");
                    chevrondown.classList.add("hidden");
                    chevronup.classList.remove("hidden");
                }
            };`,
            content: `<div class="h-full w-full">
            <!-- Code block starts -->
            <nav class="bg-white shadow xl:block hidden">
                <div class="mx-auto container px-6 py-2 xl:py-0">
                    <div class="flex items-center justify-between">
                        <div class="inset-y-0 left-0 flex items-center xl:hidden">
                            <div class="inline-flex items-center justify-center p-2 rounded-md text-gray-700 hover:text-gray-100 focus:outline-none transition duration-150 ease-in-out">
                                <div class="visible xl:hidden">
                                    <ul class="p-2 border-r bg-white absolute rounded left-0 right-0 shadow mt-8 md:mt-8 hidden">
                                        <li class="flex xl:hidden cursor-pointer text-gray-600 text-sm leading-3 tracking-normal mt-2 py-2 hover:text-indigo-700 focus:text-indigo-700 focus:outline-none">
                                            <div class="flex items-center">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-grid" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                    <path stroke="none" d="M0 0h24v24H0z"></path>
                                                    <rect x="4" y="4" width="6" height="6" rx="1"></rect>
                                                    <rect x="14" y="4" width="6" height="6" rx="1"></rect>
                                                    <rect x="4" y="14" width="6" height="6" rx="1"></rect>
                                                    <rect x="14" y="14" width="6" height="6" rx="1"></rect>
                                                </svg>
                                                <span class="ml-2 font-bold">Dashboard</span>
                                            </div>
                                        </li>
                                        <li class="flex xl:hidden flex-col cursor-pointer text-gray-600 text-sm leading-3 tracking-normal py-2 hover:text-indigo-700 focus:text-indigo-700 focus:outline-none flex justify-center" onclick="dropdownHandler(this)">
                                            <div class="flex items-center">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-puzzle" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                    <path stroke="none" d="M0 0h24v24H0z"></path>
                                                    <path d="M4 7h3a1 1 0 0 0 1 -1v-1a2 2 0 0 1 4 0v1a1 1 0 0 0 1 1h3a1 1 0 0 1 1 1v3a1 1 0 0 0 1 1h1a2 2 0 0 1 0 4h-1a1 1 0 0 0 -1 1v3a1 1 0 0 1 -1 1h-3a1 1 0 0 1 -1 -1v-1a2 2 0 0 0 -4 0v1a1 1 0 0 1 -1 1h-3a1 1 0 0 1 -1 -1v-3a1 1 0 0 1 1 -1h1a2 2 0 0 0 0 -4h-1a1 1 0 0 1 -1 -1v-3a1 1 0 0 1 1 -1"></path>
                                                </svg>
                                                <span class="ml-2 font-bold">Products</span>
                                            </div>
                                        </li>
                                        <li class="flex xl:hidden cursor-pointer text-gray-600 text-sm leading-3 tracking-normal py-2 hover:text-indigo-700 flex items-center focus:text-indigo-700 focus:outline-none">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-compass" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                <path stroke="none" d="M0 0h24v24H0z"></path>
                                                <polyline points="8 16 10 10 16 8 14 14 8 16"></polyline>
                                                <circle cx="12" cy="12" r="9"></circle>
                                            </svg>
                                            <span class="ml-2 font-bold">Performance</span>
                                        </li>
                                        <li class="border-b border-gray-300 flex xl:hidden cursor-pointer text-gray-600 text-sm leading-3 tracking-normal pt-2 pb-4 hover:text-indigo-700 flex items-center focus:text-indigo-700 focus:outline-none">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-code" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                <path stroke="none" d="M0 0h24v24H0z"></path>
                                                <polyline points="7 8 3 12 7 16"></polyline>
                                                <polyline points="17 8 21 12 17 16"></polyline>
                                                <line x1="14" y1="4" x2="10" y2="20"></line>
                                            </svg>
                                            <span class="ml-2 font-bold">Deliverables</span>
                                        </li>
                                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal mt-2 py-2 hover:text-indigo-700 flex items-center focus:text-indigo-700 focus:outline-none">
                                            <div class="flex items-center">
                                                <div class="w-12 cursor-pointer flex text-sm border-2 border-transparent rounded focus:outline-none focus:border-white transition duration-150 ease-in-out">
                                                    <img class="rounded h-10 w-10 object-cover" src="https://tuk-cdn.s3.amazonaws.com/assets/components/horizontal_navigation/hn_1.png" alt="logo" />
                                                </div>
                                                <p class="text-sm ml-2 cursor-pointer">Jane Doe</p>
                                                <div class="sm:ml-2 text-white relative">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-chevron-down cursor-pointer" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                        <path stroke="none" d="M0 0h24v24H0z"></path>
                                                        <polyline points="6 9 12 15 18 9"></polyline>
                                                    </svg>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal py-2 hover:text-indigo-700 focus:text-indigo-700 focus:outline-none">
                                            <div class="flex items-center">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-user" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                    <path stroke="none" d="M0 0h24v24H0z" />
                                                    <circle cx="12" cy="7" r="4" />
                                                    <path d="M6 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2" />
                                                </svg>
                                                <span class="ml-2">Profile</span>
                                            </div>
                                        </li>
                                    </ul>
                                    <svg onclick="MenuHandler(this,true)" aria-haspopup="true" aria-label="Main Menu" xmlns="http://www.w3.org/2000/svg" class="show-m-menu icon icon-tabler icon-tabler-menu" width="28" height="28" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z"></path>
                                        <line x1="4" y1="8" x2="20" y2="8"></line>
                                        <line x1="4" y1="16" x2="20" y2="16"></line>
                                    </svg>
                                </div>
                                <div class="hidden close-m-menu text-gray-700" onclick="MenuHandler(this,false)">
                                    <svg aria-label="Close" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" />
                                        <line x1="18" y1="6" x2="6" y2="18" />
                                        <line x1="6" y1="6" x2="18" y2="18" />
                                    </svg>
                                </div>
                            </div>
                        </div>
                        <div class="flex w-full sm:w-auto items-center sm:items-stretch justify-end sm:justify-start">
                            <div class="flex items-center">
                                <svg aria-label="Home" id="logo" enable-background="new 0 0 300 300" height="44" viewBox="0 0 300 300" width="43" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <g>
                                        <path
                                            fill="#4c51bf"
                                            d="m234.735 35.532c-8.822 0-16 7.178-16 16s7.178 16 16 16 16-7.178 16-16-7.178-16-16-16zm0 24c-4.412 0-8-3.588-8-8s3.588-8 8-8 8 3.588 8 8-3.588 8-8 8zm-62.529-14c0-2.502 2.028-4.53 4.53-4.53s4.53 2.028 4.53 4.53c0 2.501-2.028 4.529-4.53 4.529s-4.53-2.027-4.53-4.529zm89.059 60c0 2.501-2.028 4.529-4.53 4.529s-4.53-2.028-4.53-4.529c0-2.502 2.028-4.53 4.53-4.53s4.53 2.029 4.53 4.53zm-40.522-5.459-88-51.064c-1.242-.723-2.773-.723-4.016 0l-88 51.064c-1.232.715-1.992 2.033-1.992 3.459v104c0 1.404.736 2.705 1.938 3.428l88 52.936c.635.381 1.35.572 2.062.572s1.428-.191 2.062-.572l88-52.936c1.201-.723 1.938-2.023 1.938-3.428v-104c0-1.426-.76-2.744-1.992-3.459zm-90.008-42.98 80.085 46.47-52.95 31.289-23.135-13.607v-21.713c0-2.209-1.791-4-4-4s-4 1.791-4 4v21.713l-26.027 15.309c-1.223.719-1.973 2.029-1.973 3.447v29.795l-52 30.727v-94.688zm0 198.707-80.189-48.237 51.467-30.412 24.723 14.539v19.842c0 2.209 1.791 4 4 4s4-1.791 4-4v-19.842l26.027-15.307c1.223-.719 1.973-2.029 1.973-3.447v-31.667l52-30.728v94.729z"
                                        />
                                    </g>
                                </svg>
                                <h2 class="hidden sm:block text-base text-gray-700 font-bold leading-normal pl-3">The North</h2>
                            </div>
                        </div>
                        <div class="flex">
                            <div class="hidden xl:flex md:mr-6 xl:mr-16">
                                <a href="javascript: void(0)" class="flex px-5 items-center py-6 text-sm leading-5 text-gray-700 hover:bg-gray-100 focus:bg-gray-100 focus:outline-none transition duration-150 ease-in-out">
                                    <span class="mr-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-grid" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                            <path stroke="none" d="M0 0h24v24H0z"></path>
                                            <rect x="4" y="4" width="6" height="6" rx="1"></rect>
                                            <rect x="14" y="4" width="6" height="6" rx="1"></rect>
                                            <rect x="4" y="14" width="6" height="6" rx="1"></rect>
                                            <rect x="14" y="14" width="6" height="6" rx="1"></rect>
                                        </svg>
                                    </span>
                                    Dashboard
                                </a>
                                <a href="javascript: void(0)" class="flex px-5 items-center py-6 text-sm leading-5 text-gray-700 hover:bg-gray-100 focus:bg-gray-100 focus:outline-none transition duration-150 ease-in-out">
                                    <span class="mr-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-puzzle" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                            <path stroke="none" d="M0 0h24v24H0z"></path>
                                            <path d="M4 7h3a1 1 0 0 0 1 -1v-1a2 2 0 0 1 4 0v1a1 1 0 0 0 1 1h3a1 1 0 0 1 1 1v3a1 1 0 0 0 1 1h1a2 2 0 0 1 0 4h-1a1 1 0 0 0 -1 1v3a1 1 0 0 1 -1 1h-3a1 1 0 0 1 -1 -1v-1a2 2 0 0 0 -4 0v1a1 1 0 0 1 -1 1h-3a1 1 0 0 1 -1 -1v-3a1 1 0 0 1 1 -1h1a2 2 0 0 0 0 -4h-1a1 1 0 0 1 -1 -1v-3a1 1 0 0 1 1 -1"></path>
                                        </svg>
                                    </span>
                                    Products
                                </a>
                                <a href="javascript: void(0)" class="flex px-5 items-center py-6 text-sm leading-5 text-gray-700 hover:bg-gray-100 focus:bg-gray-100 focus:outline-none transition duration-150 ease-in-out">
                                    <span class="mr-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-compass" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                            <path stroke="none" d="M0 0h24v24H0z"></path>
                                            <polyline points="8 16 10 10 16 8 14 14 8 16"></polyline>
                                            <circle cx="12" cy="12" r="9"></circle>
                                        </svg>
                                    </span>
                                    Performance
                                </a>
                                <a href="javascript: void(0)" class="flex px-5 items-center py-6 text-sm leading-5 text-gray-700 hover:bg-gray-100 focus:bg-gray-100 focus:outline-none transition duration-150 ease-in-out">
                                    <span class="mr-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-code" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                            <path stroke="none" d="M0 0h24v24H0z"></path>
                                            <polyline points="7 8 3 12 7 16"></polyline>
                                            <polyline points="17 8 21 12 17 16"></polyline>
                                            <line x1="14" y1="4" x2="10" y2="20"></line>
                                        </svg>
                                    </span>
                                    Deliverables
                                </a>
                            </div>
                            <div class="hidden xl:flex items-center">
                                <div class="relative md:mr-6 my-2">
                                    <button class="focus:outline-none bg-gray-100 border-gray-300 border transition duration-150 ease-in-out hover:bg-gray-300 rounded text-gray-600 px-5 py-2 text-xs">Manage</button>
                                </div>
                                <div class="ml-6 relative">
                                    <div class="flex items-center relative" onclick="dropdownHandler(this)">
                                        <ul class="p-2 w-40 border-r bg-white absolute rounded right-0 shadow top-0 mt-16 hidden">
                                            <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal py-2 hover:text-indigo-700 focus:text-indigo-700 focus:outline-none">
                                                <div class="flex items-center">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-user" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                        <path stroke="none" d="M0 0h24v24H0z" />
                                                        <circle cx="12" cy="7" r="4" />
                                                        <path d="M6 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2" />
                                                    </svg>
                                                    <span class="ml-2">My Profile</span>
                                                </div>
                                            </li>
                                            <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal mt-2 py-2 hover:text-indigo-700 focus:text-indigo-700 focus:outline-none flex items-center">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-help" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                    <path stroke="none" d="M0 0h24v24H0z" />
                                                    <circle cx="12" cy="12" r="9" />
                                                    <line x1="12" y1="17" x2="12" y2="17.01" />
                                                    <path d="M12 13.5a1.5 1.5 0 0 1 1 -1.5a2.6 2.6 0 1 0 -3 -4" />
                                                </svg>
                                                <span class="ml-2">Help Center</span>
                                            </li>
                                            <li class="cursor-pointer text-gray-600 text-sm leading-3 tracking-normal mt-2 py-2 hover:text-indigo-700 flex items-center focus:text-indigo-700 focus:outline-none">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-settings" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                    <path stroke="none" d="M0 0h24v24H0z" />
                                                    <path d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 0 0 2.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 0 0 1.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 0 0 -1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 0 0 -2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 0 0 -2.573 -1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 0 0 -1.065 -2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 0 0 1.066 -2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z" />
                                                    <circle cx="12" cy="12" r="3" />
                                                </svg>
                                                <span class="ml-2">Account Settings</span>
                                            </li>
                                        </ul>
                                        <div class="cursor-pointer flex text-sm border-2 border-transparent rounded-full focus:outline-none focus:border-white transition duration-150 ease-in-out">
                                            <img class="rounded-full h-10 w-10 object-cover" src="https://tuk-cdn.s3.amazonaws.com/assets/components/horizontal_navigation/hn_2.png" alt="logo" />
                                        </div>
                                        <div class="ml-2 text-gray-600">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-chevron-down cursor-pointer" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                <path stroke="none" d="M0 0h24v24H0z"></path>
                                                <polyline points="6 9 12 15 18 9"></polyline>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
            <nav>
                <div class="py-4 px-6 w-full flex xl:hidden justify-between items-center bg-white fixed top-0 z-40">
                    <div class="w-24">
                        <svg xmlns="http://www.w3.org/2000/svg" width="43" height="44" viewBox="0 0 43 44" fill="none">
                            <path
                                fill-rule="evenodd"
                                clip-rule="evenodd"
                                d="M37.8735 0C36.1688 0 34.7818 1.37956 34.7818 3.0751C34.7818 4.77063 36.1688 6.15019 37.8735 6.15019C39.5782 6.15019 40.9653 4.77063 40.9653 3.0751C40.9653 1.37956 39.5782 0 37.8735 0ZM37.8735 4.61264C37.021 4.61264 36.3277 3.92305 36.3277 3.0751C36.3277 2.22714 37.021 1.53755 37.8735 1.53755C38.7261 1.53755 39.4194 2.22714 39.4194 3.0751C39.4194 3.92305 38.7261 4.61264 37.8735 4.61264ZM26.6663 1.0513C26.1828 1.0513 25.7909 1.44107 25.7909 1.92193C25.7909 2.4028 26.1828 2.79238 26.6663 2.79238C27.1497 2.79238 27.5416 2.40261 27.5416 1.92193C27.5416 1.44107 27.1497 1.0513 26.6663 1.0513ZM43 13.4535C43 13.9342 42.6081 14.324 42.1247 14.324C41.6412 14.324 41.2493 13.9342 41.2493 13.4535C41.2493 12.9727 41.6412 12.5829 42.1247 12.5829C42.6081 12.5829 43 12.9729 43 13.4535ZM18.1654 2.59019L35.1698 12.4044C35.4079 12.5418 35.5548 12.7951 35.5548 13.0692V33.0573C35.5548 33.3273 35.4123 33.5772 35.1803 33.7161L18.1758 43.8901C18.0533 43.9633 17.915 44 17.7774 44C17.6398 44 17.5016 43.9633 17.3789 43.8901L0.374484 33.7161C0.142219 33.5772 0 33.3271 0 33.0573V13.0692C0 12.7951 0.146857 12.5418 0.384919 12.4044L17.3894 2.59019C17.6296 2.45124 17.9254 2.45124 18.1654 2.59019ZM17.7774 4.14388L33.2524 13.0751L23.0207 19.0887L18.5503 16.4735V12.3004C18.5503 11.8758 18.2042 11.5316 17.7774 11.5316C17.3505 11.5316 17.0044 11.8758 17.0044 12.3004V16.4735L11.9752 19.4158C11.7389 19.554 11.5939 19.8057 11.5939 20.0783V25.8047L1.54586 31.7102V13.5118L17.7774 4.14388ZM2.28227 33.0632L17.7774 42.3341L34.0091 32.6225V14.4162L23.961 20.322V26.4081C23.961 26.6807 23.8161 26.9325 23.5798 27.0706L18.5505 30.0125V33.826C18.5505 34.2506 18.2044 34.5948 17.7776 34.5948C17.3507 34.5948 17.0046 34.2506 17.0046 33.826V30.0125L12.2274 27.2182L2.28227 33.0632Z"
                                fill="#667EEA"
                            />
                        </svg>
                    </div>
                    <div class="flex items-center">
                        <div class="relative mr-6">
                            <button class="focus:outline-none bg-gray-100 border-gray-300 border transition duration-150 ease-in-out hover:bg-gray-300 rounded text-gray-600 px-5 py-2 text-xs">Manage</button>
                        </div>
                        <div id="menu" class="text-gray-800" onclick="sidebarHandler(true)">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-menu-2" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                <line x1="4" y1="6" x2="20" y2="6" />
                                <line x1="4" y1="12" x2="20" y2="12" />
                                <line x1="4" y1="18" x2="20" y2="18" />
                            </svg>
                        </div>
                    </div>
                </div>
                <!--Mobile responsive sidebar-->
                <div class="absolute w-full h-full transform -translate-x-full z-40 xl:hidden" id="mobile-nav">
                    <div class="bg-gray-800 opacity-50 w-full h-full" onclick="sidebarHandler(false)"></div>
                    <div class="w-64 z-40 fixed overflow-y-auto z-40 top-0 bg-white shadow h-full flex-col justify-between xl:hidden pb-4 transition duration-150 ease-in-out">
                        <div class="px-6 h-full">
                            <div class="flex flex-col justify-between h-full w-full">
                                <div>
                                    <div class="mt-6 flex w-full items-center justify-between">
                                        <div class="flex items-center justify-between w-full">
                                            <div class="flex items-center">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="43" height="44" viewBox="0 0 43 44" fill="none">
                                                    <path
                                                        fill-rule="evenodd"
                                                        clip-rule="evenodd"
                                                        d="M37.8735 0C36.1688 0 34.7818 1.37956 34.7818 3.0751C34.7818 4.77063 36.1688 6.15019 37.8735 6.15019C39.5782 6.15019 40.9653 4.77063 40.9653 3.0751C40.9653 1.37956 39.5782 0 37.8735 0ZM37.8735 4.61264C37.021 4.61264 36.3277 3.92305 36.3277 3.0751C36.3277 2.22714 37.021 1.53755 37.8735 1.53755C38.7261 1.53755 39.4194 2.22714 39.4194 3.0751C39.4194 3.92305 38.7261 4.61264 37.8735 4.61264ZM26.6663 1.0513C26.1828 1.0513 25.7909 1.44107 25.7909 1.92193C25.7909 2.4028 26.1828 2.79238 26.6663 2.79238C27.1497 2.79238 27.5416 2.40261 27.5416 1.92193C27.5416 1.44107 27.1497 1.0513 26.6663 1.0513ZM43 13.4535C43 13.9342 42.6081 14.324 42.1247 14.324C41.6412 14.324 41.2493 13.9342 41.2493 13.4535C41.2493 12.9727 41.6412 12.5829 42.1247 12.5829C42.6081 12.5829 43 12.9729 43 13.4535ZM18.1654 2.59019L35.1698 12.4044C35.4079 12.5418 35.5548 12.7951 35.5548 13.0692V33.0573C35.5548 33.3273 35.4123 33.5772 35.1803 33.7161L18.1758 43.8901C18.0533 43.9633 17.915 44 17.7774 44C17.6398 44 17.5016 43.9633 17.3789 43.8901L0.374484 33.7161C0.142219 33.5772 0 33.3271 0 33.0573V13.0692C0 12.7951 0.146857 12.5418 0.384919 12.4044L17.3894 2.59019C17.6296 2.45124 17.9254 2.45124 18.1654 2.59019ZM17.7774 4.14388L33.2524 13.0751L23.0207 19.0887L18.5503 16.4735V12.3004C18.5503 11.8758 18.2042 11.5316 17.7774 11.5316C17.3505 11.5316 17.0044 11.8758 17.0044 12.3004V16.4735L11.9752 19.4158C11.7389 19.554 11.5939 19.8057 11.5939 20.0783V25.8047L1.54586 31.7102V13.5118L17.7774 4.14388ZM2.28227 33.0632L17.7774 42.3341L34.0091 32.6225V14.4162L23.961 20.322V26.4081C23.961 26.6807 23.8161 26.9325 23.5798 27.0706L18.5505 30.0125V33.826C18.5505 34.2506 18.2044 34.5948 17.7776 34.5948C17.3507 34.5948 17.0046 34.2506 17.0046 33.826V30.0125L12.2274 27.2182L2.28227 33.0632Z"
                                                        fill="#667EEA"
                                                    ></path>
                                                </svg>
                                                <p class="text-base md:text-2xl text-gray-800 ml-3">The North</p>
                                            </div>
                                            <div id="cross" class="text-gray-800" onclick="sidebarHandler(false)">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-x" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                    <path stroke="none" d="M0 0h24v24H0z"></path>
                                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="f-m-m">
                                        <a class="cursor-pointer">
                                            <li class="text-gray-800 pt-10">
                                                <div class="flex items-center">
                                                    <div class="w-6 h-6 md:w-8 md:h-8 text-indigo-700">
                                                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-grid" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                            <path stroke="none" d="M0 0h24v24H0z"></path>
                                                            <rect x="4" y="4" width="6" height="6" rx="1"></rect>
                                                            <rect x="14" y="4" width="6" height="6" rx="1"></rect>
                                                            <rect x="4" y="14" width="6" height="6" rx="1"></rect>
                                                            <rect x="14" y="14" width="6" height="6" rx="1"></rect>
                                                        </svg>
                                                    </div>
                                                    <p class="text-indigo-700 xl:text-base text-base ml-3">Dashboard</p>
                                                </div>
                                            </li>
                                        </a>
                                        <a class="cursor-pointer">
                                            <li class="text-gray-800 pt-8">
                                                <div class="flex items-center justify-between">
                                                    <div class="flex items-center">
                                                        <div class="w-6 h-6 md:w-8 md:h-8 text-gray-800">
                                                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-puzzle" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                                <path stroke="none" d="M0 0h24v24H0z"></path>
                                                                <path d="M4 7h3a1 1 0 0 0 1 -1v-1a2 2 0 0 1 4 0v1a1 1 0 0 0 1 1h3a1 1 0 0 1 1 1v3a1 1 0 0 0 1 1h1a2 2 0 0 1 0 4h-1a1 1 0 0 0 -1 1v3a1 1 0 0 1 -1 1h-3a1 1 0 0 1 -1 -1v-1a2 2 0 0 0 -4 0v1a1 1 0 0 1 -1 1h-3a1 1 0 0 1 -1 -1v-3a1 1 0 0 1 1 -1h1a2 2 0 0 0 0 -4h-1a1 1 0 0 1 -1 -1v-3a1 1 0 0 1 1 -1"></path>
                                                            </svg>
                                                        </div>
                                                        <p class="text-gray-800 xl:text-base md:text-2xl text-base ml-3">Products</p>
                                                    </div>
                                                </div>
                                            </li>
                                        </a>
                                        <a class="cursor-pointer">
                                            <li class="text-gray-800 pt-8">
                                                <div class="flex items-center">
                                                    <div class="w-6 h-6 md:w-8 md:h-8 text-gray-800">
                                                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-compass" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                            <path stroke="none" d="M0 0h24v24H0z"></path>
                                                            <polyline points="8 16 10 10 16 8 14 14 8 16"></polyline>
                                                            <circle cx="12" cy="12" r="9"></circle>
                                                        </svg>
                                                    </div>
                                                    <p class="text-gray-800 xl:text-base md:text-2xl text-base ml-3">Performance</p>
                                                </div>
                                            </li>
                                        </a>
                                        <li class="text-gray-800 pt-8 cursor-pointer">
                                            <div class="flex items-center justify-between">
                                                <div class="flex items-center">
                                                    <div class="w-6 h-6 md:w-8 md:h-8 text-gray-800">
                                                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-code" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                            <path stroke="none" d="M0 0h24v24H0z"></path>
                                                            <polyline points="7 8 3 12 7 16"></polyline>
                                                            <polyline points="17 8 21 12 17 16"></polyline>
                                                            <line x1="14" y1="4" x2="10" y2="20"></line>
                                                        </svg>
                                                    </div>
                                                    <p class="text-gray-800 xl:text-base md:text-2xl text-base ml-3">Deliverables</p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="w-full pt-4">
                                    <div class="flex justify-center mb-4 w-full">
                                        <div class="relative w-full">
                                            <div class="text-gray-500 absolute ml-4 inset-0 m-auto w-4 h-4">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-search" width="16" height="16" viewBox="0 0 24 24" stroke-width="1" stroke="#A0AEC0" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                    <path stroke="none" d="M0 0h24v24H0z"></path>
                                                    <circle cx="10" cy="10" r="7"></circle>
                                                    <line x1="21" y1="21" x2="15" y2="15"></line>
                                                </svg>
                                            </div>
                                            <input class="bg-gray-100 focus:outline-none rounded w-full text-sm text-gray-500 pl-10 py-2" type="text" placeholder="Search" />
                                        </div>
                                    </div>
                                    <div class="border-t border-gray-300">
                                        <div class="w-full flex items-center justify-between pt-1">
                                            <div class="flex items-center">
                                                <img alt="profile-pic" src="https://tuk-cdn.s3.amazonaws.com/assets/components/boxed_layout/bl_1.png" class="w-8 h-8 rounded-md" />
                                                <p class=" text-gray-800 text-base leading-4 ml-2">Jane Doe</p>
                                            </div>
                                            <ul class="flex">
                                                <li class="cursor-pointer text-gray-800 pt-5 pb-3">
                                                    <div class="w-6 h-6 md:w-8 md:h-8">
                                                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-messages" viewBox="0 0 24 24" stroke-width="1" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                            <path stroke="none" d="M0 0h24v24H0z"></path>
                                                            <path d="M21 14l-3 -3h-7a1 1 0 0 1 -1 -1v-6a1 1 0 0 1 1 -1h9a1 1 0 0 1 1 1v10"></path>
                                                            <path d="M14 15v2a1 1 0 0 1 -1 1h-7l-3 3v-10a1 1 0 0 1 1 -1h2"></path>
                                                        </svg>
                                                    </div>
                                                </li>
                                                <li class="cursor-pointer text-gray-800 pt-5 pb-3 pl-3">
                                                    <div class="w-6 h-6 md:w-8 md:h-8">
                                                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-bell" viewBox="0 0 24 24" stroke-width="1" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                            <path stroke="none" d="M0 0h24v24H0z"></path>
                                                            <path d="M10 5a2 2 0 0 1 4 0a7 7 0 0 1 4 6v3a4 4 0 0 0 2 3h-16a4 4 0 0 0 2 -3v-3a7 7 0 0 1 4 -6"></path>
                                                            <path d="M9 17v1a3 3 0 0 0 6 0v-1"></path>
                                                        </svg>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
            
            <!-- Code block ends -->
        </div>
    `
        }
    }
]