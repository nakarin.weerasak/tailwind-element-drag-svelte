import Component from "./Component.svelte";


function alertMessage(props: any) {
    if (typeof props === 'string') props = { title: props }

    const dialog = new Component({
        target: document.body,
        props,
        intro: true,
    });

    dialog.$on('destroy', () => {
        dialog.$destroy
    })

    return dialog;
}

const AlertMyTemplate = {
    show: show
}


async function show(message: string) {
    const dialogConst = alertMessage(message);
    const resp = await dialogConst.promise;
    dialogConst.$destroy();
    return resp;
}

export default AlertMyTemplate;
