// import b1s from '/assets/images/column1 1.svg'
// import b2s from '/assets/images/column2 1.svg'
// import b3s from '/assets/images/column3 1.svg'
// import b4s from '/assets/images/column3-7 1.svg'
const b5s = '/assets/images/basic/image1.svg'
const b6s = '/assets/images/basic/link1.svg'
const b7s = '/assets/images/basic/map1.svg'
const b8s = '/assets/images/basic/text1.svg'
const b9s = '/assets/images/basic/video1.svg'

export function loadBasicBlocks(editor, opt = {}) {
    const c = opt;
    let bm = editor.BlockManager;
    const category = 'Basic';

    const toAdd = (type: any) => true // blocks.indexOf(name) >= 0;

    toAdd('text') &&
        bm.add('text', {
            label: `<img src="${b8s}"/>`,
            category: category,
            // attributes: { class: 'gjs-fonts gjs-f-text' },
            content: {
                type: 'text',
                // content: '<span class="leading-relaxed">Insert your styled text here</span>',
                content: 'Insert your text here',
                style: { padding: '10px' },
                activeOnRender: 1
            }
        });

    toAdd('link') &&
        bm.add('link', {
            label: `<img src="${b6s}"/>`,
            category: category,
            // attributes: { class: 'fa fa-link' },
            content: {
                type: 'link',
                content: 'Link',
                style: { color: '#6366f1' }
            }
        });

    toAdd('image') &&
        bm.add('image', {
            label: `<img src="${b5s}"/>`,
            category: category,
            // attributes: { class: 'gjs-fonts gjs-f-image' },
            content: {
                style: { color: 'black' },
                type: 'image',
                activeOnRender: 1
            }
        });

    toAdd('video') &&
        bm.add('video', {
            label: `<img src="${b9s}"/>`,
            category: category,
            // attributes: { class: 'fa fa-youtube-play' },
            content: {
                type: 'video',
                src: 'img/video2.webm',
                style: {
                    height: '350px',
                    width: '615px'
                }
            }
        });

    toAdd('map') &&
        bm.add('map', {
            label: `<img src="${b7s}"/>`,
            category: category,
            // attributes: { class: 'fa fa-map-o' },
            content: {
                type: 'map',
                style: { height: '350px' }
            }
        });


    // toAdd('script') &&
    //     bm.add('script', {
    //         label: `<img src="${b9s}"/>`,
    //         category: category,
    //         // attributes: { class: 'fa fa-youtube-play' },
    //         content: {
    //             type: 'comp-with-js',
    //         }
    //     });
}