import { source as b1 } from '../data/blog-1'
const b1s = './assets/images/blog-2.svg'
import { source as b2 } from '../data/blog-2'
const b2s = './assets/images/blog-1.svg'
import { source as b3 } from '../data/blog-3'
const b3s = './assets/images/blog-3.svg'
import { source as b4 } from '../data/blog-4'
const b4s = './assets/images/blog-4.svg'
import { source as b5 } from '../data/blog-5'
const b5s = './assets/images/blog-5.svg'

import { source as c1 } from '../data/contact-1'
const c1s = './assets/images/contact-1.svg'
import { source as c2 } from '../data/contact-2'
const c2s = './assets/images/contact-2.svg'
import { source as c3 } from '../data/contact-3'
const c3s = './assets/images/contact-3.svg'

import { source as d1 } from '../data/content-1'
const d1s = './assets/images/content-1.svg'
import { source as d2 } from '../data/content-2'
const d2s = './assets/images/content-2.svg'
import { source as d3 } from '../data/content-3'
const d3s = './assets/images/content-3.svg'
import { source as d4 } from '../data/content-4'
const d4s = './assets/images/content-4.svg'
import { source as d5 } from '../data/content-5'
const d5s = './assets/images/content-5.svg'
import { source as d6 } from '../data/content-6'
const d6s = './assets/images/content-6.svg'
import { source as d7 } from '../data/content-7'
const d7s = './assets/images/content-7.svg'
import { source as d8 } from '../data/content-8'
const d8s = './assets/images/content-8.svg'

import { source as a1 } from '../data/cta-1'
const a1s = './assets/images/cta-1.svg'
import { source as a2 } from '../data/cta-2'
const a2s = './assets/images/cta-2.svg'
import { source as a3 } from '../data/cta-3'
const a3s = './assets/images/cta-3.svg'
import { source as a4 } from '../data/cta-4'
const a4s = './assets/images/cta-4.svg'

import { source as e1 } from '../data/ecommerce-1'
const e1s = './assets/images/ecommerce-1.svg'
import { source as e2 } from '../data/ecommerce-2'
const e2s = './assets/images/ecommerce-2.svg'
import { source as e3 } from '../data/ecommerce-3'
const e3s = './assets/images/ecommerce-3.svg'

import { source as f1 } from '../data/feature-1'
const f1s = './assets/images/feature-1.svg'
import { source as f2 } from '../data/feature-2'
const f2s = './assets/images/feature-2.svg'
import { source as f3 } from '../data/feature-3'
const f3s = './assets/images/feature-3.svg'
import { source as f4 } from '../data/feature-4'
const f4s = './assets/images/feature-4.svg'
import { source as f5 } from '../data/feature-5'
const f5s = './assets/images/feature-5.svg'
import { source as f6 } from '../data/feature-6'
const f6s = './assets/images/feature-6.svg'
import { source as f7 } from '../data/feature-7'
const f7s = './assets/images/feature-7.svg'
import { source as f8 } from '../data/feature-8'
const f8s = './assets/images/feature-8.svg'

import { source as z1 } from '../data/footer-1'
const z1s = './assets/images/footer-1.svg'
import { source as z2 } from '../data/footer-2'
const z2s = './assets/images/footer-2.svg'
import { source as z3 } from '../data/footer-3'
const z3s = './assets/images/footer-3.svg'
import { source as z4 } from '../data/footer-4'
const z4s = './assets/images/footer-4.svg'
import { source as z5 } from '../data/footer-5'
const z5s = './assets/images/footer-5.svg'

import { source as g1 } from '../data/gallery-1'
const g1s = './assets/images/gallery-1.svg'
import { source as g2 } from '../data/gallery-2'
const g2s = './assets/images/gallery-2.svg'
import { source as g3 } from '../data/gallery-3'
const g3s = './assets/images/gallery-3.svg'

import { source as h1 } from '../data/header-1'
const h1s = './assets/images/header-1.svg'
import { source as h2 } from '../data/header-2'
const h2s = './assets/images/header-2.svg'
import { source as h3 } from '../data/header-3'
const h3s = './assets/images/header-3.svg'
import { source as h4 } from '../data/header-4'
const h4s = './assets/images/header-4.svg'

import { source as r1 } from '../data/hero-1'
const r1s = './assets/images/hero-1.svg'
import { source as r2 } from '../data/hero-2'
const r2s = './assets/images/hero-2.svg'
import { source as r3 } from '../data/hero-3'
const r3s = './assets/images/hero-3.svg'
import { source as r4 } from '../data/hero-4'
const r4s = './assets/images/hero-4.svg'
import { source as r5 } from '../data/hero-5'
const r5s = './assets/images/hero-5.svg'
import { source as r6 } from '../data/hero-6'
const r6s = './assets/images/hero-6.svg'

import { source as p1 } from '../data/pricing-1'
const p1s = './assets/images/pricing-1.svg'
import { source as p2 } from '../data/pricing-2'
const p2s = './assets/images/pricing-2.svg'

import { source as s1 } from '../data/statistic-1'
const s1s = './assets/images/statistic-1.svg'
import { source as s2 } from '../data/statistic-2'
const s2s = './assets/images/statistic-2.svg'
import { source as s3 } from '../data/statistic-3'
const s3s = './assets/images/statistic-3.svg'

import { source as q1 } from '../data/step-1'
const q1s = './assets/images/step-1.svg'
import { source as q2 } from '../data/step-2'
const q2s = './assets/images/step-2.svg'
import { source as q3 } from '../data/step-3'
const q3s = './assets/images/step-3.svg'

import { source as t1 } from '../data/team-1'
const t1s = './assets/images/team-1.svg'
import { source as t2 } from '../data/team-2'
const t2s = './assets/images/team-2.svg'
import { source as t3 } from '../data/team-3'
const t3s = './assets/images/team-3.svg'

import { source as m1 } from '../data/testimonial-1'
const m1s = './assets/images/testimonial-1.svg'
import { source as m2 } from '../data/testimonial-2'
const m2s = './assets/images/testimonial-2.svg'
import { source as m3 } from '../data/testimonial-3'
const m3s = './assets/images/testimonial-3.svg'

const sources = [{
    id: 'blog-block-1',
    // class: 'fa fa-map-o',
    class: '',
    label: `<img src="${b1s}"/>`,
    // label: 'b2s().outerHTML',
    content: b1,
    category: 'Blog',
    // order: 1
}, {
    id: 'blog-block-2',
    class: '',
    label: `<img src="${b2s}"/>`,
    content: b2,
    category: 'Blog',
    // order: 1
}, {
    id: 'blog-block-3',
    class: '',
    label: `<img src="${b3s}"/>`,
    content: b3,
    category: 'Blog',
    // order: 1
}, {
    id: 'blog-block-4',
    class: '',
    label: `<img src="${b4s}"/>`,
    content: b4,
    category: 'Blog',
    // order: 1
}, {
    id: 'blog-block-5',
    class: '',
    label: `<img src="${b5s}"/>`,
    content: b5,
    category: 'Blog',
    // order: 1
}, {
    id: 'contact-block-1',
    class: '',
    label: `<img src="${c1s}"/>`,
    content: c1,
    category: 'Contact',
    // order: 1
}, {
    id: 'contact-block-2',
    class: '',
    label: `<img src="${c2s}"/>`,
    content: c2,
    category: 'Contact',
    // order: 1
}, {
    id: 'contact-block-3',
    class: '',
    label: `<img src="${c3s}"/>`,
    content: c3,
    category: 'Contact',
    // order: 1
}, {
    id: 'content-block-1',
    class: '',
    label: `<img src="${d1s}"/>`,
    content: d1,
    category: 'Content',
    // order: 1
}, {
    id: 'content-block-2',
    class: '',
    label: `<img src="${d2s}"/>`,
    content: d2,
    category: 'Content',
    // order: 1
}, {
    id: 'content-block-3',
    class: '',
    label: `<img src="${d3s}"/>`,
    content: d3,
    category: 'Content',
    // order: 1
}, {
    id: 'content-block-4',
    class: '',
    label: `<img src="${d4s}"/>`,
    content: d4,
    category: 'Content',
    // order: 1
}, {
    id: 'content-block-5',
    class: '',
    label: `<img src="${d5s}"/>`,
    content: d5,
    category: 'Content',
    // order: 1
}, {
    id: 'content-block-6',
    class: '',
    label: `<img src="${d6s}"/>`,
    content: d6,
    category: 'Content',
    // order: 1
}, {
    id: 'content-block-7',
    class: '',
    label: `<img src="${d7s}"/>`,
    content: d7,
    category: 'Content',
    // order: 1
}, {
    id: 'content-block-8',
    class: '',
    label: `<img src="${d8s}"/>`,
    content: d8,
    category: 'Content',
    // order: 1
}, {
    id: 'cta-block-1',
    class: '',
    label: `<img src="${a1s}"/>`,
    content: a1,
    category: 'CTA',
    // order: 1
}, {
    id: 'cta-block-2',
    class: '',
    label: `<img src="${a2s}"/>`,
    content: a2,
    category: 'CTA',
    // order: 1
}, {
    id: 'cta-block-3',
    class: '',
    label: `<img src="${a3s}"/>`,
    content: a3,
    category: 'CTA',
    // order: 1
}, {
    id: 'cta-block-4',
    class: '',
    label: `<img src="${a4s}"/>`,
    content: a4,
    category: 'CTA',
    // order: 1
}, {
    id: 'commerce-block-1',
    class: '',
    label: `<img src="${e1s}"/>`,
    content: e1,
    category: 'Commerce',
    // order: 1
}, {
    id: 'commerce-block-2',
    class: '',
    label: `<img src="${e2s}"/>`,
    content: e2,
    category: 'Commerce',
    // order: 1
}, {
    id: 'commerce-block-3',
    class: '',
    label: `<img src="${e3s}"/>`,
    content: e3,
    category: 'Commerce',
    // order: 1
}, {
    id: 'feature-block-1',
    class: '',
    label: `<img src="${f1s}"/>`,
    content: f1,
    category: 'Features',
    // order: 1
}, {
    id: 'feature-block-2',
    class: '',
    label: `<img src="${f2s}"/>`,
    content: f2,
    category: 'Features',
    // order: 1
}, {
    id: 'feature-block-3',
    class: '',
    label: `<img src="${f3s}"/>`,
    content: f3,
    category: 'Features',
    // order: 1
}, {
    id: 'feature-block-4',
    class: '',
    label: `<img src="${f4s}"/>`,
    content: f4,
    category: 'Features',
    // order: 1
}, {
    id: 'feature-block-5',
    class: '',
    label: `<img src="${f5s}"/>`,
    content: f5,
    category: 'Features',
    // order: 1
}, {
    id: 'feature-block-6',
    class: '',
    label: `<img src="${f6s}"/>`,
    content: f6,
    category: 'Features',
    // order: 1
}, {
    id: 'feature-block-7',
    class: '',
    label: `<img src="${f7s}"/>`,
    content: f7,
    category: 'Features',
    // order: 1
}, {
    id: 'feature-block-8',
    class: '',
    label: `<img src="${f8s}"/>`,
    content: f8,
    category: 'Features',
    // order: 1
}, {
    id: 'footer-block-1',
    class: '',
    label: `<img src="${z1s}"/>`,
    content: z1,
    category: 'Footer',
    // order: 1
}, {
    id: 'footer-block-2',
    class: '',
    label: `<img src="${z2s}"/>`,
    content: z2,
    category: 'Footer',
    // order: 1
}, {
    id: 'footer-block-3',
    class: '',
    label: `<img src="${z3s}"/>`,
    content: z3,
    category: 'Footer',
    // order: 1
}, {
    id: 'footer-block-4',
    class: '',
    label: `<img src="${z4s}"/>`,
    content: z4,
    category: 'Footer',
    // order: 1
}, {
    id: 'footer-block-5',
    class: '',
    label: `<img src="${z5s}"/>`,
    content: z5,
    category: 'Footer',
    // order: 1
}, {
    id: 'gallery-block-1',
    class: '',
    label: `<img src="${g1s}"/>`,
    content: g1,
    category: 'Gallery',
    // order: 1
}, {
    id: 'gallery-block-2',
    class: '',
    label: `<img src="${g2s}"/>`,
    content: g2,
    category: 'Gallery',
    // order: 1
}, {
    id: 'gallery-block-3',
    class: '',
    label: `<img src="${g3s}"/>`,
    content: g3,
    category: 'Gallery',
    // order: 1
}, {
    id: 'header-block-1',
    class: '',
    label: `<img src="${h1s}"/>`,
    content: h1,
    category: 'Header',
    // order: 1
}, {
    id: 'header-block-2',
    class: '',
    label: `<img src="${h2s}"/>`,
    content: h2,
    category: 'Header',
    // order: 1
}, {
    id: 'header-block-3',
    class: '',
    label: `<img src="${h3s}"/>`,
    content: h3,
    category: 'Header',
    // order: 1
}, {
    id: 'header-block-4',
    class: '',
    label: `<img src="${h4s}"/>`,
    content: h4,
    category: 'Header',
    // order: 1
}, {
    id: 'hero-block-1',
    class: '',
    label: `<img src="${r1s}"/>`,
    content: r1,
    category: 'Hero',
    // order: 1
}, {
    id: 'hero-block-2',
    class: '',
    label: `<img src="${r2s}"/>`,
    content: r2,
    category: 'Hero',
    // order: 1
}, {
    id: 'hero-block-3',
    class: '',
    label: `<img src="${r3s}"/>`,
    content: r3,
    category: 'Hero',
    // order: 1
}, {
    id: 'hero-block-4',
    class: '',
    label: `<img src="${r4s}"/>`,
    content: r4,
    category: 'Hero',
    // order: 1
}, {
    id: 'hero-block-5',
    class: '',
    label: `<img src="${r5s}"/>`,
    content: r5,
    category: 'Hero',
    // order: 1
}, {
    id: 'hero-block-6',
    class: '',
    label: `<img src="${r6s}"/>`,
    content: r6,
    category: 'Hero',
    // order: 1
}, {
    id: 'pricing-block-1',
    class: '',
    label: `<img src="${p1s}"/>`,
    content: p1,
    category: 'Pricing',
    // order: 1
}, {
    id: 'pricing-block-2',
    class: '',
    label: `<img src="${p2s}"/>`,
    content: p2,
    category: 'Pricing',
    // order: 1
}, {
    id: 'statistic-block-1',
    class: '',
    label: `<img src="${s1s}"/>`,
    content: s1,
    category: 'Statistics',
    // order: 1
}, {
    id: 'statistic-block-2',
    class: '',
    label: `<img src="${s2s}"/>`,
    content: s2,
    category: 'Statistics',
    // order: 1
}, {
    id: 'statistic-block-3',
    class: '',
    label: `<img src="${s3s}"/>`,
    content: s3,
    category: 'Statistics',
    // order: 1
}, {
    id: 'step-block-1',
    class: '',
    label: `<img src="${q1s}"/>`,
    content: q1,
    category: 'Steps',
    // order: 1
}, {
    id: 'step-block-2',
    class: '',
    label: `<img src="${q2s}"/>`,
    content: q2,
    category: 'Steps',
    // order: 1
}, {
    id: 'step-block-3',
    class: '',
    label: `<img src="${q3s}"/>`,
    content: q3,
    category: 'Steps',
    // order: 1
}, {
    id: 'team-block-1',
    class: '',
    label: `<img src="${t1s}"/>`,
    content: t1,
    category: 'Team',
    // order: 1
}, {
    id: 'team-block-2',
    class: '',
    label: `<img src="${t2s}"/>`,
    content: t2,
    category: 'Team',
    // order: 1
}, {
    id: 'team-block-3',
    class: '',
    label: `<img src="${t3s}"/>`,
    content: t3,
    category: 'Team',
    // order: 1
}, {
    id: 'testimonial-block-1',
    class: '',
    label: `<img src="${m1s}"/>`,
    content: m1,
    category: 'Testimonials',
    // order: 1
}, {
    id: 'testimonial-block-2',
    class: '',
    label: `<img src="${m2s}"/>`,
    content: m2,
    category: 'Testimonials',
    // order: 1
}, {
    id: 'testimonial-block-3',
    class: '',
    label: `<img src="${m3s}"/>`,
    content: m3,
    category: 'Testimonials',
    // order: 1
}]

export const loadTailwindBlocks = (newEditor) => {
    const blockManager = newEditor.BlockManager

    sources.forEach((s: any) => {
        blockManager.add(s.id, {
            label: s.label,
            attributes: { class: s.class },
            content: s.content,
            category: { label: s.category, order: s.order, open: false },
        })
    })
}