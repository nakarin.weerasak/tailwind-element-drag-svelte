import {loadFormComponents} from './form'

export function loadComponents(editor) {
    loadFormComponents(editor)
}