export type TemplateModel = {
    id: string
    type?: string
    class?: string
    label: string
    content: any
}