import './global.postcss'
import App from './App.svelte'
import 'grapesjs/dist/css/grapes.min.css'

const app = new App({
  target: document.getElementById('app')
})

export default app
